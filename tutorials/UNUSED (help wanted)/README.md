# Unused tutorials

The tutorials in this folder are currently unused and likely outdated.

They're neither bundled with Inkscape nor included on the website.

If you can spare a few minutes, please help us by reviewing the content
of these tutorials:
- if they're still relevant, they should be updated,  made translatable
  and added to the build system
- if the're no longer  relevant, or unlikely to be useful for a wider
  audience, consider moving them out of this repository

## no-po

The tutorials in the folder "no-po" are translated versions of the
existing tutorials, but we're lacking a `.po` file to automate translations.

If you speak one of those languages, consider extracting the strings,
so the translation can be properly maintained and bundled with Inkscape.
