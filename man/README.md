# Inkscape man pages

This is where man is kept in [Plain Old Documentation (POD)](https://perldoc.perl.org/perlpod) format.

## Generating output

To generate man pages in `*pod.in` format for including them with Inkscape, in `*.html` format for displaying them on the inkscape.org website (and accessible from Help > Commandline options inside Inkscape), or to update the pot/po files for translation, you can use the following commands:

- `make all`          : Create man pages (`.pod` and `.html` files) from `inkscape.pod.in`
- `make check-input`  : Validate (non-compiled) input files
- `make check-output` : Validate (compiled) output files
- `make pot`          : Update `man.pot`
- `make po`           : Update `man.pot` and all `.po` files
- `make copy`         : Copy generated HTML man pages to the export directory
- `make clean`        : Remove all generated files
- `make version`      : Print installed versions

For help and additional usage information use `make help` and see the `README.md` file in the root directory of this repository.

## Requirements

- `po4a` (to create localized `.pod` files from `.po` files)
- `pod2html` (to create HTML files from `.pod` files)


## Adding a new translation language

To create a new translation, create a new po file by copying the
original `pod.pot` to `pod.xx.po` (where `xx` is your language code).
Then use a translation tool (such as poedit) or a text editor to
translate.
